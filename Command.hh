<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\console
{
	use nuclio\plugin\console\ConsoleException;
	
	class Command
	{
		private string $name;
		private CallbackDef $callback;
		private Map<string,ArgumentDef> $arguments;
		
		public function __construct(string $name,CallbackDef $callback)
		{
			$this->name		=$name;
			$this->callback	=$callback;
			$this->arguments=new Map(null);
		}
		
		public function addArgument(ArgumentDef $argDef):this
		{
			$name=ltrim($argDef['name'],'-');
			$aliases=$argDef['alias'];
			if (!is_null($aliases))
			{
				if (!is_array($aliases) && !$aliases instanceof Vector)
				{
					$aliases=Vector{$aliases};
				}
				
				foreach ($aliases as $i=>$alias)
				{
					$aliases[$i]=ltrim($alias,'-');
				}
			}
			$this->arguments[$name]=$argDef;
			return $this;
		}
		
		public function getArgument(string $name):?ArgumentDef
		{
			return $this->arguments->get($name);
		}
		
		public function getArgumentValues(string $name):?Vector<string>
		{
			$arg=$this->arguments->get($name);
			if (!is_null($arg))
			{
				return $arg['values'];
			}
			return null;
		}
		
		public function getArgumentValue(string $name):mixed
		{
			$values=$this->getArgumentValues($name);
			if (!is_null($values))
			{
				if ($values->count())
				{
					$value=$values[0];
					if (strtolower($value)==='true')
					{
						$value=true;
					}
					else if (strtolower($value)==='false')
					{
						$value=false;
					}
					else if (strtolower($value)==='null')
					{
						$value=null;
					}
					else if (is_numeric($value))
					{
						$value=(int)$value;
					}
					return $value;
				}
				else
				{
					return true;
				}
			}
			return null;
		}
		
		public function execute(CommandOptionsDef $options):void
		{
			foreach ($this->arguments as $argName=>$arg)
			{
				if ($arg['required'])
				{
					if (!$options->containsKey($argName))
					{
						$found	=false;
						$aliases=$arg['alias'];
						if (!is_null($aliases))
						{
							foreach ($options as $optionName=>$optionValues)
							{
								if ($aliases->linearSearch($optionName)!==-1)
								{
									$found=true;
									break;
								}
							}
						}
						if (!$found)
						{
							throw new ConsoleException(sprintf('Missing argument "%s". This is required.',$argName));
						}
					}
					$thisOption=$options->get($argName);
					if (!is_null($thisOption) && !$thisOption->count())
					{
						throw new ConsoleException(sprintf('Argument "%s" is missing a value.',$argName));
					}
				}
			}
			foreach ($options as $name=>$values)
			{
				$arg=$this->arguments->get($name);
				if (!is_null($arg))
				{
					$arg['values']=$values;
					$this->arguments->set($name,$arg);
				}
			}
			$func=$this->callback;
			$func($this);
		}
	}
}
