<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\console
{
	type ArgumentDef=shape
	(
		'name'			=>string,
		'alias'			=>?Vector<string>,
		'description'	=>string,
		'required'		=>bool,
		'values'		=>?Vector<string>
	);
	type CallbackDef=(function(Command):void);
	type CommandOptionsDef=Map<string,Vector<string>>;
}
