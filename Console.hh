<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\console
{
	require_once('types.hh');
	require_once('globals.hh');
	
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	
	<<singleton>>
	class Console extends Plugin
	{
		private Map<string,Command> $commands;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Console
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct()
		{
			parent::__construct();
			
			$this->commands=Map{};
		}
		
		public function execute():void
		{
			$input=getArgvInput();
			
			if (!is_null($input))
			{
				$input->removeKey(0);
				if ($input->count())
				{
					$this->handleInput($input);
				}
				else
				{
					$this->outputHelp();
				}
			}
			else
			{
				$this->outputHelp();
			}
		}
		
		public function addCommand(string $name,CallbackDef $callback):Command
		{
			$this->commands[$name]=new Command($name,$callback);
			return $this->commands[$name];
		}
		
		public function getCommand(string $name):?Command
		{
			return $this->commands->get($name);
		}
		
		public function commandExists(string $name):bool
		{
			return (bool)$this->getCommand($name);
		}
		
		public function executeCommand(string $name,CommandOptionsDef $options):void
		{
			$command=$this->getCommand($name);
			if ($command instanceof Command)
			{
				$command->execute($options);
			}
			else
			{
				throw new ConsoleException(sprintf('Command "%s" not found.',$command));
			}
		}
		
		private function handleInput(Vector<string> $input):void
		{
			$command=$input->get(0);
			$input->removeKey(0);
			if (!is_null($command) && $this->commandExists($command))
			{
				$options=$this->parseOptions($input);
				
				$this->executeCommand($command,$options);
			}
			else
			{
				throw new ConsoleException(sprintf('Command "%s" not found.',$command));
			}
		}
		
		private function parseOptions(Vector<string> $options):CommandOptionsDef
		{
			$optionsMap			=Map{};
			$currentOption		=null;
			$curretOptionValues	=Vector{};
			foreach ($options as $option)
			{
				if (!is_null($currentOption))
				{
					if ($this->isOption($option))
					{
						//Save it to the Map
						ltrim($currentOption,'-')
						|> $optionsMap[$$]=$curretOptionValues;
						
						//Reset current
						$currentOption		=$option;
						$curretOptionValues	=Vector{};
						continue;
					}
					else
					{
						$curretOptionValues[]=$option;
					}
				}
				elseif ($this->isOption($option))
				{
					$currentOption=$option;
					continue;
				}
				//Else ignore it.
			}
			if (!is_null($currentOption))
			{
				ltrim($currentOption,'-')
				|> $optionsMap[$$]=$curretOptionValues;
			}
			return $optionsMap;
		}
		
		private function isOption(string $option):bool
		{
			return (substr($option,0,1)=='-' || substr($option,0,2)=='--');
		}
		
		public function outputHelp():void
		{
			die('No Input. You need HELP!');
		}
	}
}
